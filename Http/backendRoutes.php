<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/interviews'], function (Router $router) {
    $router->bind('interviewsetup', function ($id) {
        return app('Modules\Interviews\Repositories\InterviewSetupRepository')->find($id);
    });
    $router->get('interviewsetups', [
        'as' => 'admin.interviews.interviewsetup.index',
        'uses' => 'InterviewSetupController@index',
        'middleware' => 'can:interviews.interviewsetups.index'
    ]);
    $router->get('interviewsetups/create', [
        'as' => 'admin.interviews.interviewsetup.create',
        'uses' => 'InterviewSetupController@create',
        'middleware' => 'can:interviews.interviewsetups.create'
    ]);
    $router->post('interviewsetups', [
        'as' => 'admin.interviews.interviewsetup.store',
        'uses' => 'InterviewSetupController@store',
        'middleware' => 'can:interviews.interviewsetups.create'
    ]);
    $router->get('interviewsetups/{interviewsetup}/edit', [
        'as' => 'admin.interviews.interviewsetup.edit',
        'uses' => 'InterviewSetupController@edit',
        'middleware' => 'can:interviews.interviewsetups.edit'
    ]);
    $router->put('interviewsetups/{interviewsetup}', [
        'as' => 'admin.interviews.interviewsetup.update',
        'uses' => 'InterviewSetupController@update',
        'middleware' => 'can:interviews.interviewsetups.edit'
    ]);
    $router->delete('interviewsetups/{interviewsetup}', [
        'as' => 'admin.interviews.interviewsetup.destroy',
        'uses' => 'InterviewSetupController@destroy',
        'middleware' => 'can:interviews.interviewsetups.destroy'
    ]);
    $router->bind('interview', function ($id) {
        return app('Modules\Interviews\Repositories\InterviewRepository')->find($id);
    });
    $router->get('interviews', [
        'as' => 'admin.interviews.interview.index',
        'uses' => 'InterviewController@index',
        'middleware' => 'can:interviews.interviews.index'
    ]);
    $router->get('interviews/create', [
        'as' => 'admin.interviews.interview.create',
        'uses' => 'InterviewController@create',
        'middleware' => 'can:interviews.interviews.create'
    ]);
    $router->post('interviews', [
        'as' => 'admin.interviews.interview.store',
        'uses' => 'InterviewController@store',
        'middleware' => 'can:interviews.interviews.create'
    ]);
    $router->get('interviews/{interview}/edit', [
        'as' => 'admin.interviews.interview.edit',
        'uses' => 'InterviewController@edit',
        'middleware' => 'can:interviews.interviews.edit'
    ]);
    $router->put('interviews/{interview}', [
        'as' => 'admin.interviews.interview.update',
        'uses' => 'InterviewController@update',
        'middleware' => 'can:interviews.interviews.edit'
    ]);
    $router->delete('interviews/{interview}', [
        'as' => 'admin.interviews.interview.destroy',
        'uses' => 'InterviewController@destroy',
        'middleware' => 'can:interviews.interviews.destroy'
    ]);
    $router->bind('interviewhistory', function ($id) {
        return app('Modules\Interviews\Repositories\InterviewHistoryRepository')->find($id);
    });
    $router->get('interviewhistories', [
        'as' => 'admin.interviews.interviewhistory.index',
        'uses' => 'InterviewHistoryController@index',
        'middleware' => 'can:interviews.interviewhistories.index'
    ]);
    $router->get('interviewhistories/create', [
        'as' => 'admin.interviews.interviewhistory.create',
        'uses' => 'InterviewHistoryController@create',
        'middleware' => 'can:interviews.interviewhistories.create'
    ]);
    $router->post('interviewhistories', [
        'as' => 'admin.interviews.interviewhistory.store',
        'uses' => 'InterviewHistoryController@store',
        'middleware' => 'can:interviews.interviewhistories.create'
    ]);
    $router->get('interviewhistories/{interviewhistory}/edit', [
        'as' => 'admin.interviews.interviewhistory.edit',
        'uses' => 'InterviewHistoryController@edit',
        'middleware' => 'can:interviews.interviewhistories.edit'
    ]);
    $router->put('interviewhistories/{interviewhistory}', [
        'as' => 'admin.interviews.interviewhistory.update',
        'uses' => 'InterviewHistoryController@update',
        'middleware' => 'can:interviews.interviewhistories.edit'
    ]);
    $router->delete('interviewhistories/{interviewhistory}', [
        'as' => 'admin.interviews.interviewhistory.destroy',
        'uses' => 'InterviewHistoryController@destroy',
        'middleware' => 'can:interviews.interviewhistories.destroy'
    ]);
    $router->bind('itemevaluation', function ($id) {
        return app('Modules\Interviews\Repositories\ItemEvaluationRepository')->find($id);
    });
    $router->get('itemevaluations', [
        'as' => 'admin.interviews.itemevaluation.index',
        'uses' => 'ItemEvaluationController@index',
        'middleware' => 'can:interviews.itemevaluations.index'
    ]);
    $router->get('itemevaluations/create', [
        'as' => 'admin.interviews.itemevaluation.create',
        'uses' => 'ItemEvaluationController@create',
        'middleware' => 'can:interviews.itemevaluations.create'
    ]);
    $router->post('itemevaluations', [
        'as' => 'admin.interviews.itemevaluation.store',
        'uses' => 'ItemEvaluationController@store',
        'middleware' => 'can:interviews.itemevaluations.create'
    ]);
    $router->get('itemevaluations/{itemevaluation}/edit', [
        'as' => 'admin.interviews.itemevaluation.edit',
        'uses' => 'ItemEvaluationController@edit',
        'middleware' => 'can:interviews.itemevaluations.edit'
    ]);
    $router->put('itemevaluations/{itemevaluation}', [
        'as' => 'admin.interviews.itemevaluation.update',
        'uses' => 'ItemEvaluationController@update',
        'middleware' => 'can:interviews.itemevaluations.edit'
    ]);
    $router->delete('itemevaluations/{itemevaluation}', [
        'as' => 'admin.interviews.itemevaluation.destroy',
        'uses' => 'ItemEvaluationController@destroy',
        'middleware' => 'can:interviews.itemevaluations.destroy'
    ]);
// append




});
