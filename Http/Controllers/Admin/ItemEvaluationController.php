<?php

namespace Modules\Interviews\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Interviews\Entities\ItemEvaluation;
use Modules\Interviews\Http\Requests\CreateItemEvaluationRequest;
use Modules\Interviews\Http\Requests\UpdateItemEvaluationRequest;
use Modules\Interviews\Repositories\ItemEvaluationRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ItemEvaluationController extends AdminBaseController
{
    /**
     * @var ItemEvaluationRepository
     */
    private $itemevaluation;

    public function __construct(ItemEvaluationRepository $itemevaluation)
    {
        parent::__construct();

        $this->itemevaluation = $itemevaluation;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $itemevaluations = $this->itemevaluation->all();

        return view('interviews::admin.itemevaluations.index', compact('itemevaluations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('interviews::admin.itemevaluations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateItemEvaluationRequest $request
     * @return Response
     */
    public function store(CreateItemEvaluationRequest $request)
    {

        $total=ItemEvaluation::sum("max_points");
        if($total+$request->max_points>150)
          return redirect()->back()->withInput($request->input())->with("error","La sumatoria de puntos de todos los items supera los 150 puntos.");
        $this->itemevaluation->create($request->all());

        return redirect()->route('admin.interviews.itemevaluation.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('interviews::itemevaluations.title.itemevaluations')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  ItemEvaluation $itemevaluation
     * @return Response
     */
    public function edit(ItemEvaluation $itemevaluation)
    {
        return view('interviews::admin.itemevaluations.edit', compact('itemevaluation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ItemEvaluation $itemevaluation
     * @param  UpdateItemEvaluationRequest $request
     * @return Response
     */
    public function update(ItemEvaluation $itemevaluation, UpdateItemEvaluationRequest $request)
    {
      $total=ItemEvaluation::where('id',"!=",$itemevaluation->id)->sum("max_points");
      if($total+$request->max_points>150)
        return redirect()->back()->withInput($request->input())->with("error","La sumatoria de puntos de todos los items supera los 150 puntos.");
        $this->itemevaluation->update($itemevaluation, $request->all());

        return redirect()->route('admin.interviews.itemevaluation.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('interviews::itemevaluations.title.itemevaluations')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ItemEvaluation $itemevaluation
     * @return Response
     */
    public function destroy(ItemEvaluation $itemevaluation)
    {
        $this->itemevaluation->destroy($itemevaluation);

        return redirect()->route('admin.interviews.itemevaluation.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('interviews::itemevaluations.title.itemevaluations')]));
    }
}
