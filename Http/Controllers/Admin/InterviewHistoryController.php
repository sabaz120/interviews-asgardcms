<?php

namespace Modules\Interviews\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Interviews\Entities\InterviewHistory;
use Modules\Interviews\Http\Requests\CreateInterviewHistoryRequest;
use Modules\Interviews\Http\Requests\UpdateInterviewHistoryRequest;
use Modules\Interviews\Repositories\InterviewHistoryRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class InterviewHistoryController extends AdminBaseController
{
    /**
     * @var InterviewHistoryRepository
     */
    private $interviewhistory;

    public function __construct(InterviewHistoryRepository $interviewhistory)
    {
        parent::__construct();

        $this->interviewhistory = $interviewhistory;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$interviewhistories = $this->interviewhistory->all();

        return view('interviews::admin.interviewhistories.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('interviews::admin.interviewhistories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateInterviewHistoryRequest $request
     * @return Response
     */
    public function store(CreateInterviewHistoryRequest $request)
    {
        $this->interviewhistory->create($request->all());

        return redirect()->route('admin.interviews.interviewhistory.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('interviews::interviewhistories.title.interviewhistories')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  InterviewHistory $interviewhistory
     * @return Response
     */
    public function edit(InterviewHistory $interviewhistory)
    {
        return view('interviews::admin.interviewhistories.edit', compact('interviewhistory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  InterviewHistory $interviewhistory
     * @param  UpdateInterviewHistoryRequest $request
     * @return Response
     */
    public function update(InterviewHistory $interviewhistory, UpdateInterviewHistoryRequest $request)
    {
        $this->interviewhistory->update($interviewhistory, $request->all());

        return redirect()->route('admin.interviews.interviewhistory.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('interviews::interviewhistories.title.interviewhistories')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  InterviewHistory $interviewhistory
     * @return Response
     */
    public function destroy(InterviewHistory $interviewhistory)
    {
        $this->interviewhistory->destroy($interviewhistory);

        return redirect()->route('admin.interviews.interviewhistory.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('interviews::interviewhistories.title.interviewhistories')]));
    }
}
