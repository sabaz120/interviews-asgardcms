<?php

namespace Modules\Interviews\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Interviews\Entities\InterviewSetup;
use Modules\Interviews\Http\Requests\CreateInterviewSetupRequest;
use Modules\Interviews\Http\Requests\UpdateInterviewSetupRequest;
use Modules\Interviews\Repositories\InterviewSetupRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class InterviewSetupController extends AdminBaseController
{
    /**
     * @var InterviewSetupRepository
     */
    private $interviewsetup;

    public function __construct(InterviewSetupRepository $interviewsetup)
    {
        parent::__construct();

        $this->interviewsetup = $interviewsetup;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $interviewsetups = $this->interviewsetup->all();

        return view('interviews::admin.interviewsetups.index', compact('interviewsetups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('interviews::admin.interviewsetups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateInterviewSetupRequest $request
     * @return Response
     */
    public function store(CreateInterviewSetupRequest $request)
    {
        $this->interviewsetup->create($request->all());

        return redirect()->route('admin.interviews.interviewsetup.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('interviews::interviewsetups.title.interviewsetups')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  InterviewSetup $interviewsetup
     * @return Response
     */
    public function edit(InterviewSetup $interviewsetup)
    {
        return view('interviews::admin.interviewsetups.edit', compact('interviewsetup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  InterviewSetup $interviewsetup
     * @param  UpdateInterviewSetupRequest $request
     * @return Response
     */
    public function update(InterviewSetup $interviewsetup, UpdateInterviewSetupRequest $request)
    {
        $this->interviewsetup->update($interviewsetup, $request->all());

        return redirect()->route('admin.interviews.interviewsetup.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('interviews::interviewsetups.title.interviewsetups')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  InterviewSetup $interviewsetup
     * @return Response
     */
    public function destroy(InterviewSetup $interviewsetup)
    {
        $this->interviewsetup->destroy($interviewsetup);

        return redirect()->route('admin.interviews.interviewsetup.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('interviews::interviewsetups.title.interviewsetups')]));
    }
}
