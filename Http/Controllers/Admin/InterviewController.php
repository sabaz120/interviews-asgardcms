<?php

namespace Modules\Interviews\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Interviews\Entities\Interview;
use Modules\Interviews\Entities\InterviewDetail;
use Modules\Interviews\Entities\InterviewHistory;
use Modules\Preselection\Entities\Aspirants;
use Modules\Interviews\Http\Requests\CreateInterviewRequest;
use Modules\Interviews\Http\Requests\UpdateInterviewRequest;
use Modules\Interviews\Repositories\InterviewRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class InterviewController extends AdminBaseController
{
    /**
     * @var InterviewRepository
     */
    private $interview;

    public function __construct(InterviewRepository $interview)
    {
        parent::__construct();

        $this->interview = $interview;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $interviews = $this->interview->all();

        return view('interviews::admin.interviews.index', compact('interviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('interviews::admin.interviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateInterviewRequest $request
     * @return Response
     */
    public function store(CreateInterviewRequest $request)
    {
        $this->interview->create($request->all());

        return redirect()->route('admin.interviews.interview.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('interviews::interviews.title.interviews')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Interview $interview
     * @return Response
     */
    public function edit(Interview $interview)
    {
      $itemsEvaluation=\Modules\Interviews\Entities\ItemEvaluation::all();
        return view('interviews::admin.interviews.edit', compact('interview','itemsEvaluation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Interview $interview
     * @param  UpdateInterviewRequest $request
     * @return Response
     */
    public function update(Interview $interview, UpdateInterviewRequest $request)
    {
        $items=json_decode($request->items);
        $data=$request->all();
        if($interview->status==1){
          $status=1;
          $points=0;
          foreach($items as $item){
            InterviewDetail::create([
              "interview_id"=>$interview->id,
              "item_name"=>$item->name,
              "score_result"=>$item->score,
              "max_points"=>$item->max_points
            ]);
            $points=$points+$item->score;
          }//foreach items
          if($points>=$request->minimum_approval){
            $status=2;//Aprobado
            Aspirants::where('id',$interview->aspirant_id)->update(["status"=>6]);
          }
          else{
            Aspirants::where('id',$interview->aspirant_id)->update(["status"=>7]);
            $status=3;//Rechazado
          }
          InterviewHistory::create([
            "interview_id"=>$interview->id,
            "status"=>$status,
            "comment"=>$request->comment
          ]);
          $data['status']=$status;
        }//If interview status 1
        $this->interview->update($interview, $data);


        return redirect()->route('admin.interviews.interview.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('interviews::interviews.title.interviews')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Interview $interview
     * @return Response
     */
    public function destroy(Interview $interview)
    {
        $this->interview->destroy($interview);

        return redirect()->route('admin.interviews.interview.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('interviews::interviews.title.interviews')]));
    }
}
