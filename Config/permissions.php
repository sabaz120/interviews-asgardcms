<?php

return [
    'interviews.interviewsetups' => [
        'index' => 'interviews::interviewsetups.list resource',
        'create' => 'interviews::interviewsetups.create resource',
        'edit' => 'interviews::interviewsetups.edit resource',
        'destroy' => 'interviews::interviewsetups.destroy resource',
    ],
    'interviews.interviews' => [
        'index' => 'interviews::interviews.list resource',
        'create' => 'interviews::interviews.create resource',
        'edit' => 'interviews::interviews.edit resource',
        'destroy' => 'interviews::interviews.destroy resource',
    ],
    'interviews.interviewhistories' => [
        'index' => 'interviews::interviewhistories.list resource',
        'create' => 'interviews::interviewhistories.create resource',
        'edit' => 'interviews::interviewhistories.edit resource',
        'destroy' => 'interviews::interviewhistories.destroy resource',
    ],
    'interviews.itemevaluations' => [
        'index' => 'interviews::itemevaluations.list resource',
        'create' => 'interviews::itemevaluations.create resource',
        'edit' => 'interviews::itemevaluations.edit resource',
        'destroy' => 'interviews::itemevaluations.destroy resource',
    ],
// append




];
