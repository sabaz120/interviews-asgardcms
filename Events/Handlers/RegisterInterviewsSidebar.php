<?php

namespace Modules\Interviews\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterInterviewsSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('interviews::interviews.title.interviews'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     $this->auth->hasAccess('interviews.interviewsetups.index')
                );
                $item->item(trans('interviews::interviewsetups.title.interviewsetups'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    // $item->append('admin.interviews.interviewsetup.create');
                    $item->route('admin.interviews.interviewsetup.index');
                    $item->authorize(
                        $this->auth->hasAccess('interviews.interviewsetups.index')
                    );
                });
                $item->item(trans('interviews::interviews.title.interviews'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    // $item->append('admin.interviews.interview.create');
                    $item->route('admin.interviews.interview.index');
                    $item->authorize(
                        $this->auth->hasAccess('interviews.interviews.index')
                    );
                });
                $item->item(trans('interviews::itemevaluations.title.itemevaluations'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.interviews.itemevaluation.create');
                    $item->route('admin.interviews.itemevaluation.index');
                    $item->authorize(
                        $this->auth->hasAccess('interviews.itemevaluations.index')
                    );
                });
// append




            });
        });

        return $menu;
    }
}
