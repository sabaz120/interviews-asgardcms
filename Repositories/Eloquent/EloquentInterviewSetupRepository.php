<?php

namespace Modules\Interviews\Repositories\Eloquent;

use Modules\Interviews\Repositories\InterviewSetupRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentInterviewSetupRepository extends EloquentBaseRepository implements InterviewSetupRepository
{
}
