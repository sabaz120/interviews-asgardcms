<?php

namespace Modules\Interviews\Repositories\Eloquent;

use Modules\Interviews\Repositories\InterviewHistoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentInterviewHistoryRepository extends EloquentBaseRepository implements InterviewHistoryRepository
{
}
