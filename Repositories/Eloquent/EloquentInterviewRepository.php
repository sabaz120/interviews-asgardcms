<?php

namespace Modules\Interviews\Repositories\Eloquent;

use Modules\Interviews\Repositories\InterviewRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentInterviewRepository extends EloquentBaseRepository implements InterviewRepository
{
}
