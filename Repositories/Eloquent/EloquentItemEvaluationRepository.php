<?php

namespace Modules\Interviews\Repositories\Eloquent;

use Modules\Interviews\Repositories\ItemEvaluationRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentItemEvaluationRepository extends EloquentBaseRepository implements ItemEvaluationRepository
{
}
