<?php

namespace Modules\Interviews\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface InterviewSetupRepository extends BaseRepository
{
}
