<?php

namespace Modules\Interviews\Repositories\Cache;

use Modules\Interviews\Repositories\ItemEvaluationRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheItemEvaluationDecorator extends BaseCacheDecorator implements ItemEvaluationRepository
{
    public function __construct(ItemEvaluationRepository $itemevaluation)
    {
        parent::__construct();
        $this->entityName = 'interviews.itemevaluations';
        $this->repository = $itemevaluation;
    }
}
