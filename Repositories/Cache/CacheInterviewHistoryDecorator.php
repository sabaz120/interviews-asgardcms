<?php

namespace Modules\Interviews\Repositories\Cache;

use Modules\Interviews\Repositories\InterviewHistoryRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheInterviewHistoryDecorator extends BaseCacheDecorator implements InterviewHistoryRepository
{
    public function __construct(InterviewHistoryRepository $interviewhistory)
    {
        parent::__construct();
        $this->entityName = 'interviews.interviewhistories';
        $this->repository = $interviewhistory;
    }
}
