<?php

namespace Modules\Interviews\Repositories\Cache;

use Modules\Interviews\Repositories\InterviewRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheInterviewDecorator extends BaseCacheDecorator implements InterviewRepository
{
    public function __construct(InterviewRepository $interview)
    {
        parent::__construct();
        $this->entityName = 'interviews.interviews';
        $this->repository = $interview;
    }
}
