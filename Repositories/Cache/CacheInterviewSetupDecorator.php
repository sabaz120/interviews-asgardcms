<?php

namespace Modules\Interviews\Repositories\Cache;

use Modules\Interviews\Repositories\InterviewSetupRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheInterviewSetupDecorator extends BaseCacheDecorator implements InterviewSetupRepository
{
    public function __construct(InterviewSetupRepository $interviewsetup)
    {
        parent::__construct();
        $this->entityName = 'interviews.interviewsetups';
        $this->repository = $interviewsetup;
    }
}
