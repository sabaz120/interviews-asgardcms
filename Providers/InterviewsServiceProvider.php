<?php

namespace Modules\Interviews\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Interviews\Events\Handlers\RegisterInterviewsSidebar;

class InterviewsServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterInterviewsSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('interviewsetups', array_dot(trans('interviews::interviewsetups')));
            $event->load('interviews', array_dot(trans('interviews::interviews')));
            $event->load('interviewhistories', array_dot(trans('interviews::interviewhistories')));
            $event->load('itemevaluations', array_dot(trans('interviews::itemevaluations')));
            // append translations




        });
    }

    public function boot()
    {
        $this->publishConfig('interviews', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Interviews\Repositories\InterviewSetupRepository',
            function () {
                $repository = new \Modules\Interviews\Repositories\Eloquent\EloquentInterviewSetupRepository(new \Modules\Interviews\Entities\InterviewSetup());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Interviews\Repositories\Cache\CacheInterviewSetupDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Interviews\Repositories\InterviewRepository',
            function () {
                $repository = new \Modules\Interviews\Repositories\Eloquent\EloquentInterviewRepository(new \Modules\Interviews\Entities\Interview());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Interviews\Repositories\Cache\CacheInterviewDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Interviews\Repositories\InterviewHistoryRepository',
            function () {
                $repository = new \Modules\Interviews\Repositories\Eloquent\EloquentInterviewHistoryRepository(new \Modules\Interviews\Entities\InterviewHistory());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Interviews\Repositories\Cache\CacheInterviewHistoryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Interviews\Repositories\ItemEvaluationRepository',
            function () {
                $repository = new \Modules\Interviews\Repositories\Eloquent\EloquentItemEvaluationRepository(new \Modules\Interviews\Entities\ItemEvaluation());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Interviews\Repositories\Cache\CacheItemEvaluationDecorator($repository);
            }
        );
// add bindings




    }
}
