<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterviewsInterviewSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interviews__interviewsetups', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string("init_day");//Tuesday
            $table->string("end_day");//Tuesday
            //$table->time("init_hour")->nullable();
            //$table->time("end_hour")->nullable();
            $table->integer("limit_per_day");//20
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interviews__interviewsetups');
    }
}
