<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinimumApprovalAndInterviewerFieldsInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('interviews__interviews', function (Blueprint $table) {
          $table->integer("minimum_approval")->default(0);
          $table->string("interviewer_name")->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('interviews__interviews', function (Blueprint $table) {
        $table->dropColumn('minimum_approval');
        $table->dropColumn('interviewer_name');
      });
    }
}
