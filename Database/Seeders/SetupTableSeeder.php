<?php

namespace Modules\Interviews\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Interviews\Entities\InterviewSetup;

class SetupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        InterviewSetup::create([
            "init_day"=>"Monday",
            "end_day"=>"Friday",
            "limit_per_day"=>15
        ]);
    }
}
