<?php

return [
    'list resource' => 'Listado de entrevistas',
    'create resource' => 'Crear entrevistas',
    'edit resource' => 'Editar entrevistas',
    'destroy resource' => 'Eliminar entrevistas',
    'title' => [
        'interviews' => 'Entrevistas',
        'create interview' => 'Crear entrevistas',
        'edit interview' => 'Editar entrevistas',
    ],
    'button' => [
        'create interview' => 'Crear entrevistas',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
