<?php

return [
    'list resource' => 'Lista de configuración de entrevista',
    'create resource' => 'Create interviewsetups',
    'edit resource' => 'Editar configuración de entrevista',
    'destroy resource' => 'Borrar configuración de entrevista',
    'title' => [
        'interviewsetups' => 'Configuración de entrevista',
        'create interviewsetup' => 'Crear una configuración de entrevista',
        'edit interviewsetup' => 'Editar una configuración de entrevista',
    ],
    'button' => [
        'create interviewsetup' => 'Crear una configuración de entrevista',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'settings' => [
        "studies"=>"Porcentaje mínimo requerido "
    ],
];
