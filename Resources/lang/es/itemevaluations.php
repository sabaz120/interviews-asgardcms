<?php

return [
    'list resource' => 'Lista de items de evaluación',
    'create resource' => 'Crear items de evaluación',
    'edit resource' => 'Editar items de evaluación',
    'destroy resource' => 'Borrar items de evaluación',
    'title' => [
        'itemevaluations' => 'Items de evaluación',
        'create itemevaluation' => 'Crear un item de evaluación',
        'edit itemevaluation' => 'Editar un item de evaluación',
    ],
    'button' => [
        'create itemevaluation' => 'Crear un item de evaluación',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
