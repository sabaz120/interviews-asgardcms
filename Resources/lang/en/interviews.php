<?php

return [
    'list resource' => 'List interviews',
    'create resource' => 'Create interviews',
    'edit resource' => 'Edit interviews',
    'destroy resource' => 'Destroy interviews',
    'title' => [
        'interviews' => 'Interview',
        'create interview' => 'Create a interview',
        'edit interview' => 'Edit a interview',
    ],
    'button' => [
        'create interview' => 'Create a interview',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
