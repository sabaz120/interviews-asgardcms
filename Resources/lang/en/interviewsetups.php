<?php

return [
    'list resource' => 'List interviewsetups',
    'create resource' => 'Create interviewsetups',
    'edit resource' => 'Edit interviewsetups',
    'destroy resource' => 'Destroy interviewsetups',
    'title' => [
        'interviewsetups' => 'InterviewSetup',
        'create interviewsetup' => 'Create a interviewsetup',
        'edit interviewsetup' => 'Edit a interviewsetup',
    ],
    'button' => [
        'create interviewsetup' => 'Create a interviewsetup',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'settings' => [
        "studies"=>"Percentage for minimum required studies"
    ],
];
