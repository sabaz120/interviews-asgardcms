<?php

return [
    'list resource' => 'List itemevaluations',
    'create resource' => 'Create itemevaluations',
    'edit resource' => 'Edit itemevaluations',
    'destroy resource' => 'Destroy itemevaluations',
    'title' => [
        'itemevaluations' => 'ItemEvaluation',
        'create itemevaluation' => 'Create a itemevaluation',
        'edit itemevaluation' => 'Edit a itemevaluation',
    ],
    'button' => [
        'create itemevaluation' => 'Create a itemevaluation',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
