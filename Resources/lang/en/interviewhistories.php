<?php

return [
    'list resource' => 'List interviewhistories',
    'create resource' => 'Create interviewhistories',
    'edit resource' => 'Edit interviewhistories',
    'destroy resource' => 'Destroy interviewhistories',
    'title' => [
        'interviewhistories' => 'InterviewHistory',
        'create interviewhistory' => 'Create a interviewhistory',
        'edit interviewhistory' => 'Edit a interviewhistory',
    ],
    'button' => [
        'create interviewhistory' => 'Create a interviewhistory',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
