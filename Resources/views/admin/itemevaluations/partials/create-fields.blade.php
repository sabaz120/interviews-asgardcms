<div class="box-body">
  <div class="row">
    <div class="col-md-10">
      <label>Nombre</label>
      <input type="text" class="form-control" name="name" value="{{old('name')}}" required>
    </div>
    <div class="col-md-2">
      <label>Máximo de puntos</label>
      <input type="number" class="form-control" name="max_points" required value="{{old('max_points',1)}}">
    </div>
  </div>
</div>
