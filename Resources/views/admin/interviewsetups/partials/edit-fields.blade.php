<div class="box-body">
  <div class="row">
    <div class="col-md-4">
      <label>Desde</label>
      <select class="form-control" name="init_day">
        <option value="Monday" @if($interviewsetup->init_day == "Monday") selected @endif>Lunes</option>
        <option value="Tuesday" @if($interviewsetup->init_day == "Tuesday") selected @endif>Martes</option>
        <option value="Wednesday" @if($interviewsetup->init_day == "Wednesday") selected @endif>Miercoles</option>
        <option value="Thursday" @if($interviewsetup->init_day == "Thursday") selected @endif>Jueves</option>
        <option value="Friday" @if($interviewsetup->init_day == "Friday") selected @endif>Viernes</option>
        <option value="Saturday" @if($interviewsetup->init_day == "Saturday") selected @endif>Sabado</option>
        <option value="Sunday" @if($interviewsetup->init_day == "Sunday") selected @endif>Domingo</option>
      </select>
    </div>
    <div class="col-md-4">
      <label>Hasta</label>
      <select class="form-control" name="end_day">
        <option value="Monday" @if($interviewsetup->end_day == "Monday") selected @endif>Lunes</option>
        <option value="Tuesday" @if($interviewsetup->end_day == "Tuesday") selected @endif>Martes</option>
        <option value="Wednesday" @if($interviewsetup->end_day == "Wednesday") selected @endif>Miercoles</option>
        <option value="Thursday" @if($interviewsetup->end_day == "Thursday") selected @endif>Jueves</option>
        <option value="Friday" @if($interviewsetup->end_day == "Friday") selected @endif>Viernes</option>
        <option value="Saturday" @if($interviewsetup->end_day == "Saturday") selected @endif>Sabado</option>
        <option value="Sunday" @if($interviewsetup->end_day == "Sunday") selected @endif>Domingo</option>
      </select>
    </div>
    <div class="col-md-4">
      <label>Cantidad de entrevistas por día</label>
      <input type="number" name="limit_per_day" class="form-control" value="{{$interviewsetup->limit_per_day}}" required>
    </div>
  </div>
</div>
