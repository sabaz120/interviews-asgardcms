<div class="box-body">
  <div class="row">
    <div class="col-md-4">
      <label>Desde</label>
      <select class="form-control" name="init_day">
        <option value="Monday">Lunes</option>
        <option value="Tuesday">Martes</option>
        <option value="Wednesday">Miercoles</option>
        <option value="Thursday">Jueves</option>
        <option value="Friday">Viernes</option>
        <option value="Saturday">Sabado</option>
        <option value="Sunday">Domingo</option>
      </select>
    </div>
    <div class="col-md-4">
      <label>Hasta</label>
      <select class="form-control" name="end_day">
        <option value="Monday">Lunes</option>
        <option value="Tuesday">Martes</option>
        <option value="Wednesday">Miercoles</option>
        <option value="Thursday">Jueves</option>
        <option value="Friday">Viernes</option>
        <option value="Saturday">Sabado</option>
        <option value="Sunday">Domingo</option>
      </select>
    </div>
    <div class="col-md-4">
      <label>Cantidad de entrevistas por día</label>
      <input type="number" name="limit_per_day" class="form-control" required>
    </div>
  </div>
</div>
