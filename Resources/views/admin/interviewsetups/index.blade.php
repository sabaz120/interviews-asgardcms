@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('interviews::interviewsetups.title.interviewsetups') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('interviews::interviewsetups.title.interviewsetups') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            @if(count($interviewsetups)==0)
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.interviews.interviewsetup.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('interviews::interviewsetups.button.create interviewsetup') }}
                    </a>
                </div>
            </div>
            @endif
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Fecha Inicio</th>
                                <th>Fecha Fin</th>
                                <th>Cantidad de aspirantes por dia</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($interviewsetups)): ?>
                            <?php foreach ($interviewsetups as $interviewsetup): ?>
                            <tr>
                              <td>
                                @if($interviewsetup->init_day=="Monday")
                                  Lunes
                                @elseif($interviewsetup->init_day=="Tuesday")
                                  Martes
                                @elseif($interviewsetup->init_day=="Wednesday")
                                  Miercoles
                                @elseif($interviewsetup->init_day=="Thursday")
                                  Jueves
                                @elseif($interviewsetup->init_day=="Friday")
                                  Viernes
                                @elseif($interviewsetup->init_day=="Saturday")
                                  Sabado
                                @elseif($interviewsetup->init_day=="Sunday")
                                  Domingo
                                @endif
                              </td>
                              <td>
                                @if($interviewsetup->end_day=="Monday")
                                  Lunes
                                @elseif($interviewsetup->end_day=="Tuesday")
                                  Martes
                                @elseif($interviewsetup->end_day=="Wednesday")
                                  Miercoles
                                @elseif($interviewsetup->end_day=="Thursday")
                                  Jueves
                                @elseif($interviewsetup->end_day=="Friday")
                                  Viernes
                                @elseif($interviewsetup->end_day=="Saturday")
                                  Sabado
                                @elseif($interviewsetup->end_day=="Sunday")
                                  Domingo
                                @endif
                              </td>
                              <td>{{ $interviewsetup->limit_per_day }}</td>
                                <td>
                                    <a href="{{ route('admin.interviews.interviewsetup.edit', [$interviewsetup->id]) }}">
                                        {{ $interviewsetup->created_at }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.interviews.interviewsetup.edit', [$interviewsetup->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <!-- <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.interviews.interviewsetup.destroy', [$interviewsetup->id]) }}"><i class="fa fa-trash"></i></button> -->
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Fecha Inicio</th>
                                <th>Fecha Fin</th>
                                <th>Cantidad de aspirantes por dia</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('interviews::interviewsetups.title.create interviewsetup') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.interviews.interviewsetup.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
