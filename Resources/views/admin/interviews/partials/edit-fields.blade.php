<style media="screen">
.bg-danger{
  background-color: #ecf0f5 !important;
}
textarea {
  resize: none !important;
}
</style>

<div class="box-body">
  @if($interview->status == 1)
    @include('interviews::admin.interviews.partials.status_1')
  @elseif($interview->status ==2 || $interview->status ==3)
    @include('interviews::admin.interviews.partials.status_2')
  @endif
</div>
@push('js-stack')


<script type="text/javascript">
var result={!!$itemsEvaluation!!};
var total=0;
var ratingValue="Insuficiente";
var valorCualitativo="No aprobado";
$('#minimum_approval_qua').val('Insuficiente');

function ratingScale(total){
  if (total>=0 && total<=29) {
    ratingValue='Insuficiente';
    $('#qualitativeValue').html(ratingValue);
    $('#minimum_approval_qua').val(ratingValue);

  }else if(total>=30 && total<=59){
    ratingValue='Regular';
    $('#qualitativeValue').html(ratingValue);
      $('#minimum_approval_qua').val(ratingValue);
  }else if (total>=60 && total<=89) {
    ratingValue='Bueno';
    $('#qualitativeValue').html(ratingValue);
      $('#minimum_approval_qua').val(ratingValue);
  }else if (total>=90 && total<=119) {
    ratingValue='Muy Bueno';
    $('#qualitativeValue').html(ratingValue);
      $('#minimum_approval_qua').val(ratingValue);
  }else if (total>=120 && total<=150) {
    ratingValue='Excelente';
    $('#qualitativeValue').html(ratingValue);
      $('#minimum_approval_qua').val(ratingValue);
  }
}

function tableItems(){
  var html="";
  var html2="";
  var result_max_points=0;
  for(var i=0;i<result.length;i++){
    result_max_points+=result[i].max_points;
    html+="<tr>";
    html+='<td>'+[i] +'</td>';
    html+='<td colspan="2">'+result[i].name+'</td>';
    html+='<td class="col-md-1 text-center">'+result[i].max_points+'</td>';
    html+='<td class="col-md-1 text-center"> <input min="0"  class="form-control result" data-max_points="'+result[i].max_points+'"   data-position="'+i+'" type="number" > </td>';
    html+="</tr>";
  }//for

  html2+="<tr>";
  html2+='<td rowspan="2" style="border-right:0px;" ></td>';
  html2+='<td rowspan="2" class="text-right" style="border-left:0px;"> <h2></h2> </td>';
  html2+='<td  class="text-right col-md-1"><h4>Total Cuantitativo</4></td>';
  html2+='<td class="text-center">'+result_max_points+'</td>';
  html2+='<td id="totalValue" class="text-center">'+total+'</td>';

  html2+="</tr>";

  html2+="<tr>";
  html2+='<td  class="text-center col-md-1"><h4>Total <br> Cualitativo</4> </td>';
  html2+='<td id="qualitativeValue" colspan="2" class="text-center">'+ratingValue+'</td>';
  html2+="</tr>";

  $('#tableItems tbody').html(html);
  $('#tableItems tfoot').html(html2);
}

function getPosition(index,value){
  var result2= result[index].score=value;
  $("#items").val(JSON.stringify(result))
  resultValue();

}


function validateResult(){
  var flag=0;
  for (var i = 0; i < result.length; i++) {
    if (result[i].score ==undefined) {
      flag=1;
      toastr.error("Debe ingresar el resultado del factor:  "+result[i].name)
    }
  }
  if (flag ==1) {
    return false;
  }else {
    return true;
  }
}

function resultValue(){
  total=0;
  var html3="";
  for (var i = 0; i < result.length; i++) {
    if (result[i].score !=undefined) {
      total+=parseInt(result[i].score);
      html3=total;
      $('#totalValue').html(html3);

      ratingScale(total);
      resultCualitativo(total);
    }
  }
}

function validateMinimum(){
  if ($('#minimum_approval').val()>150) {
    toastr.error('El Mínimo aprobatorio no debe ser mayor a 150');
    $('#minimum_approval').val(1);
  }
}

function resultCualitativo(total){
  var minimum =$('#minimum_approval').val();
    if (total<minimum) {
      valorCualitativo='No Aprobado';
      $('#totalCualitativo').html(valorCualitativo);
    }else {
      valorCualitativo='Aprobado';
      $('#totalCualitativo').html(valorCualitativo);
    }

}


$(function() {
  $('#totalCualitativo').html('No aprobado');
  tableItems();
  $('.result').change(function(){
    var value=$(this).val();
    var index= $(this).data("position");
    var max_points= $(this).data("max_points");
    getPosition(index,value);
    if (value > max_points) {
      toastr.error('El resultado no puedo ser mayor a los puntos maximos');
      $(this).val(1);
    }
  });
});

</script>
@endpush
