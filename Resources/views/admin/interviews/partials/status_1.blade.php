
  <!--FORMATO DE ENTREVISTA, ESTADO 1  -->
  <label for="">Aplicar Entrevista</label>
  <table class="data-table table table-bordered table-hover">
    <thead>
      <tr>
        <th>Nombre del aspirante</th>
        <th>
          <input type="text" name="" class="form-control" value="{{$interview->aspirant->name}} {{$interview->aspirant->second_name}} {{$interview->aspirant->last_name}} {{$interview->aspirant->second_surname}}" readonly>
        </th>
        <th>Fecha de entrevista</th>
        <th>
          <input type="date" name="date" class="form-control" value="{{$interview->date}}" required>
        </th>
      </tr>
      <tr>
        <th>Nombre del entrevistador</th>
        <th> <input type="text" name="interviewer_name" class="form-control" required> </th>
        <th>Cargo que aspira</th>
        <th><input type="text"  class="form-control" value="{{$interview->aspirant->unit->name}}" readonly></th>
      </tr>

      <tr>
        <th>Mínimo aprobatorio cuantitativo</th>
        <th><input type="number" name="minimum_approval" id="minimum_approval" class="form-control" required onchange="validateMinimum()"></th>
        <th>Mínimo aprobatorio cualitativo</th>
        <th> <input id="minimum_approval_qua" type="text" class="form-control" readonly> </th>
      </tr>
    </thead>
  </table>
  <br><br>
  <input type="hidden" name="items" id="items" value="">

  <table id="tableItems"  class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th  colspan="2">Factores a evaluar en entrevista</th>
        <th>Puntos Maximos</th>
        <th>Resultado</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    </tfoot>
  </table>

  <div class="ratingScale col-md-6">
    <table id="tableRatingScale"  class="table table-bordered table-hover">
      <thead>
        <tr>
          <th colspan="5" class="text-center " style="background-color:#dd4b39;color:white;">ESCALA DE VALORACIÓN</th>
        </tr>
      </thead>
      <tbody class="text-center">
        <tr>
          <td>0-29</td>
          <td>30-59</td>
          <td>60-89</td>
          <td>90-119</td>
          <td>120-150</td>
        </tr>
        <tr>
          <td>INSUFICIENTE</td>
          <td>REGULAR</td>
          <td>BUENO</td>
          <td>MUY BUENO</td>
          <td>EXCELENTE</td>
        </tr>
      </tbody>

    </table>
  </div>
  <!--  s-->
  <div class="col-md-6">
    <table id="tableRatingScale"  class="table table-bordered table-hover">
      <thead>
        <tr>
          <th  class="text-center " style="background-color:#dd4b39;color:white;">RESULTADO DE ENTREVISTA</th>
        </tr>
      </thead>
      <tbody class="text-center">
        <tr>
          <td id="totalCualitativo"></td>
        </tr>

      </tbody>

    </table>
   </div>
  <div class="comments col-md-12">
    <label for="">Observación</label><br>
    <small>Si no obtuvo el mínimo de puntos para aprobar entrevista,describa en forma breve su jucio y recomendaciones pendientes</small>
    <textarea  class="form-control" rows="8" cols="80" name="comment"></textarea>
  </div>
  <!--FIN FORMATO DE ENTREVISTA, ESTADO 1  -->
