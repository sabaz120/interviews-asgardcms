  <!--FORMATO DE ENTREVISTA, ESTADO 2 y 3  -->
  <label for="">Resultado de la entrevista</label>
  <table class="data-table table table-bordered table-hover">
    <thead>
      <tr>
        <th>Nombre del aspirante</th>
        <th>
          <input type="text" name="" class="form-control" value="{{$interview->aspirant->name}} {{$interview->aspirant->second_name}} {{$interview->aspirant->last_name}} {{$interview->aspirant->second_surname}}" readonly>
        </th>
        <th>Fecha de entrevista</th>
        <th>
          <input type="text" name="" class="form-control" value="{{$interview->date}}" readonly>
        </th>
      </tr>
      <tr>
        <th>Nombre del entrevistador</th>
        <th> <input type="text" name="interviewer_name" class="form-control" required  readonly value="{{$interview->interviewer_name}}" > </th>
        <th>Cargo que aspira</th>
        <th><input type="text"  class="form-control" value="{{$interview->aspirant->unit->name}}" readonly></th>
      </tr>

      <tr>
        <th>Mínimo aprobatorio cuantitativo</th>
        <th><input value="{{$interview->minimum_approval}}" type="number" name="minimum_approval" id="minimum_approval" class="form-control" required readonly></th>
        <th>Mínimo aprobatorio cualitativo</th>
        <th> <input id="minimum_approval_result" type="text" class="form-control" readonly> </th>
      </tr>
    </thead>
  </table>
  <br><br>
  <input type="hidden" name="items" id="items" value="">

  <table id="tableItemsResult"  class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th  colspan="2">Factores evaluados en la entrevista</th>
        <th>Puntos Maximos</th>
        <th>Resultado</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    </tfoot>
  </table>

  <div class="ratingScale col-md-6">
    <table id="tableRatingScale"  class="table table-bordered table-hover">
      <thead>
        <tr>
          <th colspan="5" class="text-center " style="background-color:#dd4b39;color:white;">ESCALA DE VALORACIÓN</th>
        </tr>
      </thead>
      <tbody class="text-center">
        <tr>
          <td>0-29</td>
          <td>30-59</td>
          <td>60-89</td>
          <td>90-119</td>
          <td>120-150</td>
        </tr>
        <tr>
          <td>INSUFICIENTE</td>
          <td>REGULAR</td>
          <td>BUENO</td>
          <td>MUY BUENO</td>
          <td>EXCELENTE</td>
        </tr>
      </tbody>

    </table>
  </div>
  <div class="col-md-6">
    <table id="tableRatingScale"  class="table table-bordered table-hover">
      <thead>
        <tr>
          <th  class="text-center " style="background-color:#dd4b39;color:white;">RESULTADO DE ENTREVISTA</th>
        </tr>
      </thead>
      <tbody class="text-center">
        <tr>
          @if($interview->status ==2)
            <td><h3>Aprobado</h3> </td>
          @else if($interview->status==3)
            <td> <h3>No Aprobado</h3> </td>
          @endif
        </tr>

      </tbody>

    </table>







   </div>

   <!-- HISTORIAL -->
   <table id="tableRatingScale2"  class="table table-bordered table-hover">
     <thead>
       <tr>
         <th colspan="5" class="text-center " style="background-color:#dd4b39;color:white;">HISTORIAL</th>
       </tr>
     </thead>
     <tbody class="text-center">
       <tr>
         <td>Estado</td>
         <td>Comentario</td>
         <td>Fecha</td>
       </tr>
       @foreach($interview->history as $history)
       <tr>
         @if($history->status ==2)
           <td>Aprobado</td>
          @else if($history->status ==3)
            <td>No Aprobado</td>
          @endif
           <td>{{$history->comment}}</td>
           <td>{{$history->created_at}}</td>
       </tr>
       @endforeach
     </tbody>

   </table>

  <!--FIN FORMATO DE ENTREVISTA, ESTADO 2  y 3  -->

  <script type="text/javascript">
    var result2={!!$interview->items!!};
    var totalMaxPoints=0;
    var totalResult=0;

    function GetResult(){
      for (var i = 0; i < result2.length; i++) {
        totalMaxPoints+=parseInt(result2[i].max_points);
        totalResult+=parseInt(result2[i].score_result);
      }
      $('#totalMaxPoints').html(totalMaxPoints);
      $('#totalResult').html(totalResult);
      ratingScale2(totalResult);
    }


    function tableResult(){
      var html="";
      var html2="";
      var result_max_points=0;
      for(var i=0;i<result2.length;i++){
        html+="<tr>";
        html+='<td>'+[i] +'</td>';
        html+='<td colspan="2">'+result2[i].item_name+'</td>';
        html+='<td class="col-md-1 text-center">'+result2[i].max_points+'</td>';
        html+='<td class="col-md-1 text-center">'+result2[i].score_result+'</td>';
        html+="</tr>";
      }//for

      html2+="<tr>";
      html2+='<td rowspan="2" style="border-right:0px;" ></td>';
      html2+='<td rowspan="2" class="text-right" style="border-left:0px;"> <h2></h2> </td>';
      html2+='<td  class="text-right col-md-1"><h4>Total Cuantitativo</4></td>';
      html2+='<td id="totalMaxPoints" class="text-center">'+totalMaxPoints+'</td>';
      html2+='<td id="totalResult" class="text-center">'+totalResult+'</td>';

      html2+="</tr>";

      html2+="<tr>";
      html2+='<td  class="text-center col-md-1"><h4>Total <br> Cualitativo</4> </td>';
      html2+='<td id="totalCual" colspan="2" class="text-center">'+ratingValue+'</td>';
      html2+="</tr>";


      $('#tableItemsResult tbody').html(html);
      $('#tableItemsResult tfoot').html(html2);
    }


    function ratingScale2(total){
      if (total>=0 && total<=29) {
        ratingValue='Insuficiente';
        $('#totalCual').html(ratingValue);
        $('#minimum_approval_result').val(ratingValue);

      }else if(total>=30 && total<=59){
        ratingValue='Regular';
        $('#totalCual').html(ratingValue);
          $('#minimum_approval_result').val(ratingValue);
      }else if (total>=60 && total<=89) {
        ratingValue='Bueno';
        $('#totalCual').html(ratingValue);
          $('#minimum_approval_result').val(ratingValue);
      }else if (total>=90 && total<=119) {
        ratingValue='Muy Bueno';
        $('#totalCual').html(ratingValue);
          $('#minimum_approval_result').val(ratingValue);
      }else if (total>=120 && total<=150) {
        ratingValue='Excelente';
        $('#totalCual').html(ratingValue);
          $('#minimum_approval_result').val(ratingValue);
      }
    }


    $(function() {
      tableResult();
      GetResult();
    });

  </script>
