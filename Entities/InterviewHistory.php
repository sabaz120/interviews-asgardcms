<?php

namespace Modules\Interviews\Entities;

use Illuminate\Database\Eloquent\Model;

class InterviewHistory extends Model
{

    protected $table = 'interviews__interviewhistories';
    protected $fillable = [
    	"status",
    	"comment",
    	"interview_id",
    ];
}
