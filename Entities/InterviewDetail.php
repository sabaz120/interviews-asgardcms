<?php

namespace Modules\Interviews\Entities;

use Illuminate\Database\Eloquent\Model;

class InterviewDetail extends Model
{
    protected $fillable = [
      "interview_id",
      "item_name",
      "score_result",
      "max_points",
    ];
}
