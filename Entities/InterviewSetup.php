<?php

namespace Modules\Interviews\Entities;

use Illuminate\Database\Eloquent\Model;

class InterviewSetup extends Model
{

    protected $table = 'interviews__interviewsetups';
    protected $fillable = [
    	"init_day",
    	"end_day",
    	"limit_per_day",
    ];
}
