<?php

namespace Modules\Interviews\Entities;

use Illuminate\Database\Eloquent\Model;

class ItemEvaluation extends Model
{

    protected $table = 'interviews__itemevaluations';
    protected $fillable = [
      "name",
      "max_points",
    ];
}
