<?php

namespace Modules\Interviews\Entities;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{

    protected $table = 'interviews__interviews';
    protected $fillable = [
    	"date",
    	"hour",
    	"status",
    	"aspirant_id",
    	"minimum_approval",
    	"interviewer_name",

    ];

    public function aspirant(){
      return $this->belongsTo('Modules\Preselection\Entities\Aspirants','aspirant_id');
    }

    public function items(){
      return $this->hasMany('Modules\Interviews\Entities\InterviewDetail','interview_id');
    }
    public function history(){
      return $this->hasMany('Modules\Interviews\Entities\InterviewHistory','interview_id');
    }



}
