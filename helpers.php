<?php
use Modules\Interviews\Entities\InterviewSetup;
use Modules\Interviews\Entities\Interview;

if (!function_exists('interviews__getInterviewSetup')) {

	function interviews__getInterviewSetup()
	{
		return InterviewSetup::first();
	}
}
if (!function_exists('interviews__getDateInterview')) {

	function interviews__getDateInterview($date)
	{
		//get Day = englishDayOfWeek
		//get Date = format('Y-m-d')
		$setup=InterviewSetup::first();
		if(!$setup)
			return null;
		$days=interviews__getDays($setup->init_day,$setup->end_day);
		$date=interviews__recursiveGetDate($date,$setup->limit_per_day,$days);
		return $date;
	}
}

if (!function_exists('interviews__recursiveGetDate')) {

	function interviews__recursiveGetDate($date,$perDay,$days)
	{
		if(in_array($date->englishDayOfWeek,$days)){
			$interviews=Interview::whereDate("date",$date->format("Y-m-d"))->where('status',1)->get();
			if(count($interviews)>$perDay){
				$date=$date->addDay();
				$date=interviews__recursiveGetDate($date,$perDay,$days);
			}
			return $date;
		}else{
			$date=$date->addDay();
			$date=interviews__recursiveGetDate($date,$perDay,$days);
			return $date;
		}//else
	}//interviews__recursiveGetDate()
}

if (!function_exists('interviews__getDays')) {

	function interviews__getDays($dayInit,$dayLeft)
	{
		$days=[];
		if($dayInit==$dayLeft ){
			$days[]="Monday";
			$days[]="Tuesday";
			$days[]="Wednesday";
			$days[]="Thursday";
			$days[]="Friday";
			$days[]="Saturday";
			$days[]="Sunday";
		}else if(($dayInit=="Monday" || $dayLeft=="Monday") && ($dayLeft=="Tuesday" || $dayInit=="Tuesday")){
			//Lunes a martes
			$days[]="Monday";
			$days[]="Tuesday";
		}else if(($dayInit=="Monday" || $dayLeft=="Monday") && ($dayLeft=="Wednesday" || $dayInit=="Wednesday")){
			//Lunes a martes
			$days[]="Monday";
			$days[]="Tuesday";
			$days[]="Wednesday";
		}else if(($dayInit=="Monday" || $dayLeft=="Monday") && ($dayLeft=="Thursday" || $dayInit=="Thursday")){
			//Lunes a martes
			$days[]="Monday";
			$days[]="Tuesday";
			$days[]="Wednesday";
			$days[]="Thursday";
		}else if(($dayInit=="Monday" || $dayLeft=="Monday") && ($dayLeft=="Friday" || $dayInit=="Friday")){
			//Lunes a martes
			$days[]="Monday";
			$days[]="Tuesday";
			$days[]="Wednesday";
			$days[]="Thursday";
			$days[]="Friday";
		}else if(($dayInit=="Monday" || $dayLeft=="Monday") && ($dayLeft=="Saturday" || $dayInit=="Saturday")){
			//Lunes a martes
			$days[]="Monday";
			$days[]="Tuesday";
			$days[]="Wednesday";
			$days[]="Thursday";
			$days[]="Friday";
			$days[]="Saturday";
		}else if(($dayInit=="Monday" || $dayLeft=="Monday") && ($dayLeft=="Sunday" || $dayInit=="Sunday")){
			//Lunes a martes
			$days[]="Monday";
			$days[]="Tuesday";
			$days[]="Wednesday";
			$days[]="Thursday";
			$days[]="Friday";
			$days[]="Saturday";
			$days[]="Sunday";
		}
		//Tuesday - martes
		//Wednesday - miercoles
		//Thursday - jueves
		//Friday - viernes
		//Saturday - sabado
		//Sunday - domingo
		//Monday - lunes
		return $days;
	}
}
